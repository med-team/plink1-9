plink1.9 (1.90~b7.7-241022-1) unstable; urgency=medium

  * New upstream version 1.90~b7.7-241022
  * Refresh patches
  * Update upstream changelog
  * Fix DEP3 header
  * Standards-Version: 4.7.0 (no changes needed)
  * Add more metadata in upstream/metadata

 -- Dylan Aïssi <daissi@debian.org>  Fri, 07 Feb 2025 15:31:31 +0100

plink1.9 (1.90~b7.2-231211-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Do not Build-Depends on atlas
    Closes: #1056682
  * Fix debian/get-orig-source
  * Standards-Version: 4.6.2 (routine-update)

 -- Andreas Tille <tille@debian.org>  Thu, 11 Jan 2024 16:57:57 +0100

plink1.9 (1.90~b6.26-220402-1) unstable; urgency=medium

  * New upstream release.
  * Standards-Version: 4.6.1 (no changes needed)

 -- Dylan Aïssi <daissi@debian.org>  Fri, 29 Jul 2022 16:23:52 +0200

plink1.9 (1.90~b6.24-211108-1) unstable; urgency=medium

  * Team upload.
  * New upstream bugfix release
  * Upstream provides unversioned download thus we need to read the
    version out of the source code which is done in debian/get-orig-source
    The watch file is deactivated to avoid noise with uscan
  * Standards-Version: 4.6.0 (routine-update)
  * Remove obsolete field Contact from debian/upstream/metadata (already present
    in machine-readable debian/copyright).

 -- Andreas Tille <tille@debian.org>  Thu, 03 Feb 2022 09:20:26 +0100

plink1.9 (1.90~b6.24-210606-1) unstable; urgency=medium

  * New upstream release.

 -- Dylan Aïssi <daissi@debian.org>  Sat, 21 Aug 2021 09:38:06 +0200

plink1.9 (1.90~b6.21-201019-1) unstable; urgency=medium

  * New upstream release.
  * Add patch to inject CPPFLAGS flags.

 -- Dylan Aïssi <daissi@debian.org>  Wed, 11 Nov 2020 10:42:29 +0100

plink1.9 (1.90~b6.18-200616-1) unstable; urgency=medium

  * New upstream release.
  * debhelper-compat 13 (routine-update)

 -- Dylan Aïssi <daissi@debian.org>  Mon, 06 Jul 2020 15:51:52 +0200

plink1.9 (1.90~b6.17-200428-1) unstable; urgency=medium

  * New upstream release.

 -- Dylan Aïssi <daissi@debian.org>  Fri, 29 May 2020 11:36:49 +0200

plink1.9 (1.90~b6.16-200219-1) unstable; urgency=medium

  * New upstream release.
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Dylan Aïssi <daissi@debian.org>  Tue, 21 Apr 2020 11:04:31 +0200

plink1.9 (1.90~b6.16-200217-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version: 4.5.0 (no changes needed).

 -- Dylan Aïssi <daissi@debian.org>  Wed, 19 Feb 2020 06:45:22 +0100

plink1.9 (1.90~b6.15-200121-1) unstable; urgency=medium

  * New upstream release.

 -- Dylan Aïssi <daissi@debian.org>  Wed, 22 Jan 2020 08:03:00 +0100

plink1.9 (1.90~b6.13-191130-1) unstable; urgency=medium

  * New upstream release.

 -- Dylan Aïssi <daissi@debian.org>  Sun, 15 Dec 2019 09:19:18 +0100

plink1.9 (1.90~b6.12-191028-1) unstable; urgency=medium

  * New upstream release.
  * Replace ADTTMP with AUTOPKGTEST_TMP.
  * Switch to debhelper-compat.

 -- Dylan Aïssi <daissi@debian.org>  Wed, 20 Nov 2019 07:20:09 +0100

plink1.9 (1.90~b6.11-191024-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version: 4.4.1 (no changes needed).

 -- Dylan Aïssi <daissi@debian.org>  Fri, 25 Oct 2019 22:53:20 +0200

plink1.9 (1.90~b6.10-190617-1) unstable; urgency=medium

  * New upstream release.
  * Bump debhelper compat 12.

 -- Dylan Aïssi <daissi@debian.org>  Fri, 13 Sep 2019 22:31:14 +0200

plink1.9 (1.90~b6.9-190304-2) unstable; urgency=medium

  * Bump Standards-Version: 4.4.0 (no changes needed).
  * Buster is released, so upload to unstable.

 -- Dylan Aïssi <daissi@debian.org>  Fri, 12 Jul 2019 08:31:41 +0200

plink1.9 (1.90~b6.9-190304-1) experimental; urgency=medium

  * New upstream release.
  * Update d/copyright for 2019.
  * Bump Standards-Version: 4.3.0 (no changes needed).

 -- Dylan Aïssi <daissi@debian.org>  Sat, 30 Mar 2019 13:32:09 +0100

plink1.9 (1.90~b6.6-181012-1) unstable; urgency=medium

  * New upstream release.
  * Update my email address.

 -- Dylan Aïssi <daissi@debian.org>  Thu, 01 Nov 2018 07:38:21 +0100

plink1.9 (1.90~b6.5-180913-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version: 4.2.1 (no changes needed).

 -- Dylan Aïssi <bob.dybian@gmail.com>  Sun, 16 Sep 2018 15:45:33 +0200

plink1.9 (1.90~b6.4-180807-1) unstable; urgency=medium

  * New upstream release.

 -- Dylan Aïssi <bob.dybian@gmail.com>  Wed, 22 Aug 2018 18:11:44 +0200

plink1.9 (1.90~b6.3-180717-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version: 4.2.0 (no changes needed).

 -- Dylan Aïssi <bob.dybian@gmail.com>  Fri, 03 Aug 2018 07:44:29 +0200

plink1.9 (1.90~b6.2-180612-1) unstable; urgency=medium

  * New upstream release.

 -- Dylan Aïssi <bob.dybian@gmail.com>  Wed, 04 Jul 2018 22:13:26 +0200

plink1.9 (1.90~b6.1-180528-1) unstable; urgency=medium

  * New upstream release.

 -- Dylan Aïssi <bob.dybian@gmail.com>  Mon, 04 Jun 2018 08:05:44 +0200

plink1.9 (1.90~b5.4-180410-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version: 4.1.4 (no changes needed).
  * Update VCS to salsa.

 -- Dylan Aïssi <bob.dybian@gmail.com>  Sun, 22 Apr 2018 00:50:14 +0200

plink1.9 (1.90~b5.3-180221-1) unstable; urgency=medium

  * New upstream release.

 -- Dylan Aïssi <bob.dybian@gmail.com>  Thu, 08 Mar 2018 21:50:36 +0100

plink1.9 (1.90~b5.2-180109-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version: 4.1.3 (no changes needed).
  * Bump debhelper compat 11.

 -- Dylan Aïssi <bob.dybian@gmail.com>  Fri, 12 Jan 2018 23:55:41 +0100

plink1.9 (1.90~b5-171114-1) unstable; urgency=medium

  * New upstream release.
  * Remove 01.Fix_Makefile.patch applied upstream.

 -- Dylan Aïssi <bob.dybian@gmail.com>  Thu, 30 Nov 2017 22:53:23 +0100

plink1.9 (1.90~b4.9-171013-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version: 4.1.1 (no changes needed).
  * Remove trailing whitespace characters in d/changelog.

 -- Dylan Aïssi <bob.dybian@gmail.com>  Fri, 03 Nov 2017 00:04:00 +0100

plink1.9 (1.90~b4.7-170906-1) unstable; urgency=medium

  * New upstream release.
  * Add references to registries.
  * Update 01.Fix_Makefile.patch.
  * Bump Standards-Version to 4.1.0.
  * Remove Testsuite field from d/control, now automatically added.
  * cme fix dpkg-control.

 -- Dylan Aïssi <bob.dybian@gmail.com>  Fri, 15 Sep 2017 23:53:58 +0200

plink1.9 (1.90~b4.4-170531-2) unstable; urgency=medium

  * Upload to unstable, Stretch freeze is over.

 -- Dylan Aïssi <bob.dybian@gmail.com>  Sun, 25 Jun 2017 00:19:46 +0200

plink1.9 (1.90~b4.4-170531-1) experimental; urgency=medium

  * New upstream release.

 -- Dylan Aïssi <bob.dybian@gmail.com>  Tue, 06 Jun 2017 16:08:49 +0200

plink1.9 (1.90~b4.1-170330-1) experimental; urgency=medium

  * New upstream release.
  * Update upstream website.
  * Remove 01.Makefile_config.patch & 02_Activate_Stable_Build.patch,
      inject flags using d/rules instead.
  * Add 01.Fix_Makefile.patch to take into account the injected flags.

 -- Dylan Aïssi <bob.dybian@gmail.com>  Mon, 17 Apr 2017 15:11:50 +0200

plink1.9 (1.90~b3.46-170213-1) experimental; urgency=medium

  * New upstream release.

 -- Dylan Aïssi <bob.dybian@gmail.com>  Wed, 22 Feb 2017 23:39:24 +0100

plink1.9 (1.90~b3.45-170113-1) unstable; urgency=medium

  * New upstream release.
  * Refresh patch.
  * Update d/copyright: 2017.

 -- Dylan Aïssi <bob.dybian@gmail.com>  Mon, 16 Jan 2017 23:18:12 +0100

plink1.9 (1.90~b3.44-161117-1) unstable; urgency=medium

  * New upstream release.
  * Bump debhelper 10.
  * Bump watch file v4.
  * Update patch to configure makefile for dynamic linking and hardening.

 -- Dylan Aïssi <bob.dybian@gmail.com>  Tue, 22 Nov 2016 23:08:55 +0100

plink1.9 (1.90~b3.43-161010-1) unstable; urgency=medium

  * New upstream release.
  * Update license of plink1.9 from GPL-3 to GPL-3+.
  * Install an upstream changelog.
  * Remove 03_Fix_FTBFS_GCC6.patch, fixed upstream.
  * Remove d/NEWS.Debian, redundant with d/README.Debian.

 -- Dylan Aïssi <bob.dybian@gmail.com>  Mon, 24 Oct 2016 22:47:36 +0200

plink1.9 (1.90~b3.40-160816-2) unstable; urgency=medium

  * Add an upstream patch to fix FTBFS for non-amd64 builds (Closes: #811910)
  * More simple rules file.

 -- Dylan Aïssi <bob.dybian@gmail.com>  Tue, 30 Aug 2016 22:11:27 +0200

plink1.9 (1.90~b3.40-160816-1) unstable; urgency=medium

  * New upstream release.
  * Set all hardening options.
  * Update uscan options.

 -- Dylan Aïssi <bob.dybian@gmail.com>  Wed, 24 Aug 2016 23:32:15 +0200

plink1.9 (1.90~b3.36-160416-1) unstable; urgency=low

  * New upstream release
  * Standards-Version: 3.9.8 (no changes needed)

 -- Dylan Aïssi <bob.dybian@gmail.com>  Tue, 26 Apr 2016 22:28:27 +0200

plink1.9 (1.90~b3.31-160203-1) unstable; urgency=low

  * New upstream release (Closes: #811910)
  * debian/control:
     - Standards-Version: 3.9.7 (no changes needed)
     - Fixed lintian vcs-field-uses-insecure-uri

 -- Dylan Aïssi <bob.dybian@gmail.com>  Thu, 04 Feb 2016 22:12:20 +0100

plink1.9 (1.90~b3.28-151216-1) unstable; urgency=low

  * New upstream release.

 -- Dylan Aïssi <bob.dybian@gmail.com>  Fri, 18 Dec 2015 20:37:26 +0100

plink1.9 (1.90~b3w-150903-1) unstable; urgency=low

  * New upstream release
  * debian/upstream/metadata: Update reference paper
  * debian/control: Disable build for unsupported architectures,
     thanks to Gert Wollny (Closes: #799471)
  * debian/copyright: Update

 -- Dylan Aïssi <bob.dybian@gmail.com>  Sat, 19 Sep 2015 15:44:10 +0200

plink1.9 (1.90~b3b-150117-1) unstable; urgency=low

  * New upstream release, rewrite of plink (Closes: #771154)
  * Import debian/* from plink 1.07-3
  * Updated debian/* for plink1.9

 -- Dylan Aïssi <bob.dybian@gmail.com>  Thu, 27 Nov 2014 07:54:15 +0100

plink (1.07-3) unstable; urgency=low

  * debian/patches/gcc-4.7.patch: previous patch was incomplete
    because the same problem occured in more files. Now really
    Closes: #667325

 -- Andreas Tille <tille@debian.org>  Mon, 23 Apr 2012 07:23:07 +0000

plink (1.07-2) unstable; urgency=low

  * debian/control:
     - Standards-Version: 3.9.3 (no changes needed)
     - Fixed Vcs fields
     - Removed quilt from Build-Depends
  * debian/upstream: Added citation information
  * debian/source/format: 3.0 (quilt)
  * debian/get-orig-source: Better chances to get a unique upstream
    tarball after repackaging
  * debian/copyright:
    - Rewritten to match DEP5 and verified using
       cme fix dpkg-copyright
    - Fix FSF address
  * debian/rules: Rewritten to use short dh
  * debian/patches/gcc-4.7.patch: Fix nested declaration
    Thanks for the hints on debian-mentors list to
    Rodolfo García Peñas <kix@kix.es>, Dmitry Nezhevenko <dion@inhex.net>,
    Fernando Lemos <fernandotcl@gmail.com
    Closes: 667325
  * Debhelper 9 (control+compat)

 -- Andreas Tille <tille@debian.org>  Sun, 22 Apr 2012 18:55:34 +0200

plink (1.07-1) unstable; urgency=low

  * New upstream version
  * Removed debian/patches/20_plink-1.06-gcc4.4.patch because it
    is applied upstream
  * Standards-Version: 3.8.3 (no changes needed)
  * Debhelper 7
  * Build-Depends: zlib1g-dev

 -- Andreas Tille <tille@debian.org>  Fri, 23 Oct 2009 13:35:02 +0200

plink (1.06-4) unstable; urgency=low

  * debian/rules:
    - Add support for DEB_BUILD_OPTIONS (Policy § 4.9.1).
    - Set FORCE_DYNAMIC=1 instead of patching the Makefile
      (deleted debian/patches/dynamic.patch).
  * debian/control:
    - Added myself to the uploaders.
    - Incremented Standards-Version to reflect conformance with
      Policy 3.8.2 (no changes needed).
  * documented in README.source how to find help with quilt.

 -- Charles Plessy <plessy@debian.org>  Wed, 15 Jul 2009 08:08:15 +0900

plink (1.06-3) unstable; urgency=low

  * Now really applying the patch to fix 64bit problems
    which really
    Closes: #528659

 -- Andreas Tille <tille@debian.org>  Tue, 19 May 2009 08:11:21 +0200

plink (1.06-2) unstable; urgency=low

  * Fallback to endian.h if all else fails on 64bit machines
    (Thanks to Peter Green <plugwash@p10link.net> for the patch)
    Closes: #528659

 -- Andreas Tille <tille@debian.org>  Sat, 16 May 2009 21:19:32 +0200

plink (1.06-1) unstable; urgency=low

  * New upstream version
  * debian/get-orig-source
  * Standards-Version: 3.8.1 (no changes needed)
  * Moved plink executable to /usr/lib/plink and symlinked
    /usr/bin/p-link to this location; renamed manpage accordingly
    Added explicit hint in README.Debian how to proceed if you
    need the original name.
    Closes: #503367
  * debian/patches/20_plink-1.06-gcc4.4.patch: Patch to compile
    using gcc 4.4
  * debian/rules: Make sure example data files are not executable
  * debian/patches/dynamic.patch: do not link statically

 -- Andreas Tille <tille@debian.org>  Mon, 04 May 2009 10:17:04 +0200

plink (1.03p1-1) unstable; urgency=low

  * Initial release (Closes: #490832).

 -- Steffen Moeller <moeller@debian.org>  Sun, 24 Aug 2008 17:10:55 +0200
